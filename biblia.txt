Eclesiastico
1.22: La paciencia y el dominio de sí mismo
      Un arrebato indebido no puede justificarse,
      porque el ímpetu de la pasión lleva a la propia ruina.
      El hombre paciente soporta hasta el momneto oportuno,
      pero al final se llenará de goso:
      él reserva sus palabras hasta el momento oportuno,
      y los labios de muchos proclamarán su inteligencia.

La constancia en medio de la prueba
2.2:  No te inquietes en el momento de la desgracia
2.4:  Acepta de buen grado todo lo que te suceda,
      y se paciente en las vicisitudes de tu humillación.
      Porque el oro se purifica en el fuego,
      y los que agradan a Dios, en el crisol de la humillación.

La humildad
4.18: Cuanto más grande seas, más humilde debes ser,
      y así obtendrás el favor del Señor.

La ayuda a los necesitados
4.9:  Arranca al oprimido de las manos del opresor,
      y no te acobardes al hacer justicia
4.23: No dejes de hablar cuando sea necesario,
      ni escondas tu sabiduría.
4:25: No digas nada contrario a la verdad
      y averguénzate de tu falta de instrucción.
4.29: No seas atrevido con la lengua,
      ni perezoso y descuidado en tus acciones.
4.31: No tengas la mano abierta para recibir
      y cerrada cuando hay que dar.
-------------------------------------------------------------------------------
Job
1.21:  Desnudo salí del vientre de mi madre,
       y desnudo volveré allí.
       El Señor me lo dio y el Señor me lo quitó
2.10:  Si aceptamos de Dios lo bueno,
       ¿no aceptaremos también lo malo?
7.15:  ¡Más me valdría ser estrangulado,
       prefiero la muerte a estos huesos desprecibles!
7.17:  ¿Qué es el hombre para que lo tengas tan en cuenta
       y fijes en él tu atención, visitándolo cada mañana
       y examinándolo a cada instante?
7.19:  ¿Por qué no perdonas mis ofensas
       y pasas por alto mis culpas?
       ¡Mira que muy pronto me acostaré en el polvo,
       me buscarás, y ya no existiré!
9.21:  ¡Yo soy un hombre íntegro:
       nada me importa de mí mismo
       y siento deprecio por la vida!
14.1:  El hombre, nacido de mujer
       tiene una vida breve y cargada de tomentos.
14.10: el hombre cuando muere, queda inerte;
       el mortal que expira, ¿podrá revivir?
       El agua del mar se evapora,
       un río se agota y se seca:
       así el hombre se acuesta y no se levanta;
13.14: Arriesgaré el todo por el todo
       y pondré en peligro mi vida.
       ¡Que él me mate! Ya no tengo esperanza,
       sólo quiero defender mi conduca ante él.
