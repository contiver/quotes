Preface to the second edition
- Truth is not a prostitute who throws her arms around the necks of people who
  do not want her: to the contrary, she is such a coy beauty that even someone
  who has sacrificed everything to her cannot be certain of her favours.

(§1)
- 'The world is my representation': - this holds true for every living,
  cognitive being, although only a human being can bring it to abstract,
  reflective consciousness: and if he actually does so he has become
  philosophically sound.
  
(§7)
- [Materialism] posits matter, and along with it time and space, as existing in
  themselves and ignores the relation they have to the subject (which is the
  only thing that they can all exist in). Materialism also uses the law of
  causality as a thread to guide its progress; but by taking it as an
  intrinsically existing order of things, an eternal truth, it ignores the
  understanding, since causality exists only in and for the understanding.
  Materialism attempts to find the simplest and the primary state of matter,
  and then to develop all other states out of it, ascending from mere mechanism
  to chemistry, to polarity, vegetative life, animal life. If this should
  succeed, the last link in the chain would be animal sensibility or cognition,
  which materialism would then present as a mere modification of matter, a
  material state brought about by causality. If we were to follow materialism
  this far with clear notions, we would, on reaching the summit, feel a sudden
  urge to laugh the unquenchable laughter of the Olympians: all at once, as if
  waking from a dream, we would realize that cognition, this final, painstaking
  achievement of materialism, had already been presupposed as an indispensable
  condition from the very beginning, with mere matter, that in materialism we
  had indeed imagined we were thinking about matter, while in fact all we had
  been thinking of is the subject that represents matter, the eye that sees it,
  the hand that feels it, the understanding that knows it.

(§16)
- It is rather completely contradictory to want to live without suffering, a
  contradiction also contained in the common expression 'a blessed life'.

(§27)
- what is universal, the common essence of all appearances of a particular
  sort, what must be presupposed if causal explanation is to have sense or
  meaning, is the universal force of nature, which can never be more than an
  occult quality for physics, precisely because this is where aetiological
  explanation ends and metaphysical explanation begins.

(§28)
- the will needs to live off itself because there is nothing outside of it and
  it is a hungry will. Thus, pursuit, anxiety and suffering.

(§29)
- [...] human endeavours and desires, which always delude us into believing
  that their fulfilment is the final goal of willing; but as soon as they are
  attained they no longer look the same and thus are soon forgotten, grow
  antiquated and really, if not admittedly, always laid to the side as vanished
  delusions; we are lucky enough when there is still something left to desire
  and strive after, to carry on the game of constantly passing from desire to
  satisfaction and from this to a new desire, a game whose rapid course is
  called happiness and slow course is called suffering, so that the game might
  not come to an end, showing itself to be a fearful, life-destroying boredom,
  a wearied longing without a definite object, a deadening languor.

- every particular act has a goal, but the whole of willing has none

(§38)
- All willing springs from need, and thus from lack, and thus from suffering.
  Fulfilment brings this to an end; but for every wish that is fulfilled, at
  least ten are left denied: moreover, desire lasts a long time and demands go
  on forever; fulfilment is brief and sparsely meted out. But even final
  satisfaction itself is only illusory: the fulfilled wish quickly gives way to
  a new one: the former is known to be a mistake, the latter is not yet known
  to be one. No achieved object of willing gives lasting, unwavering
  satisfaction; rather, it is only ever like the alms thrown to a beggar that
  spares his life today so that his agony can be prolonged until
  tomorrow.—Thus, as long as our consciousness is filled by our will, as long
  as we are given over to the pressure of desires with their constant hopes and
  fears, as long as we are the subject of willing, we will never have lasting
  happiness or peace.

(§39)

[speaking about a solitary region]
- Accordingly, such surroundings provide a standard for measuring our own
  intellectual value, which can be gauged by our ability to tolerate or even
  love solitude.

(§45)
- Just as beauty is the fitting presentation of the will in general through its
  purely spatial appearance, grace is the fitting presentation of the will
  through its temporal appearance, i.e. the absolutely correct and appropriate
  expression of that act of will through the movement and posture objectified
  in it.

- grace consists in the fact that every movement and posture is executed in
  the easiest, most appropriate and most comfortable manner, and is thus the
  entirely fitting expression of its intention, or the act of will, without
  anything superfluous.

(§49)
- In general, one must have value oneself in order freely and willingly to
  acknowledge value in another. This is the basis for the requirement that
  modesty accompany all merits, as well as the disproportionately loud praise
  for this virtue which alone, among all its sisters, is always added to the
  praise of anyone distinguished in some way by the person who dares to
  praise him, so as to conciliate the worthless and silence their wrath.
  For what is modesty if not false humility which someone with merits and
  advantages in a world teeming with perfidious envy uses to beg the pardon
  of those who have none? Someone who does not lay claim to merit because
  he in fact has none is being honest, not modest.

- the approval of posterity is only purchased as a rule at the cost of the
  approval of the present, and vice versa.

(§56)
- our greatest sufferings do not lie in the present, as intuitive
  representations or immediate feeling, but rather in reason, as abstract
  concepts, tormenting thoughts.

- We have long recognized that the striving that constitutes the kernel and the
  in-itself of everything is just what we call will when it appears in
  ourselves, which is where it manifests itself most clearly and in the full
  light of a most complete consciousness. When and obstacle is placed between
  it and its temporary goal, we call this inhibition suffering; on the other
  hand, the achievement of its goal is satisfaction, contentment, happiness.

- All striving comes from lack, from a dissatisfaction with one's condition,
  and is thus suffering as long as it is not satisfied; but no satisfaction is
  lasting; instead, it is only the beginning of a new striving. We see striving
  everywhere inhibited in many ways, struggling everywhere; and thus always
  suffering; there is no final goal of striving, and therefore no bounds or end
  to suffering.

(§57)
- Life itself is a sea full of reefs and maelstroms that a human being takes
  the greatest care and caution to avoid; he uses all his efforts and ingenuity
  to wend his way through, while knowing that even if he is successful, every
  step brings him closer to the greatest, the total, the inescapable and
  irreparable shipwreck, and in fact steers him right up to it, - to death:
  this is the final goal of the miserable journey and worse for him that all
  the reefs he managed to avoid.

- What keeps all living things busy and in motion is the striving to exist.
  But when existence is secured, they do not know what to do: that is why the
  second thing that sets them in motion is a striving to get rid of the burden
  of existence, not to feel it any longer, 'to kill time', i.e. to escape
  boredom.

- Boredom is certainly not an evil to be taken lightly: it will ultimately etch
  lines of true despair onto a face. It makes beings with as little love for
  each other as humans nonetheless seek each other with such intensity, and in
  this way becomes the source of sociability.

- for the most part we close our eyes to the recognition - which is like bitter
  medicine - that suffering is essential to life, and thus does not flow in
  upon us from the outside, but that all people carry within themselves an
  unconquerable source of suffering.
  
(§58)
- All satisfaction, or what is generally called happiness, is actually and
  essentially only ever negative and absolutely never positive. It is not
  something primordial that comes to us from out of itself, it must always be
  the satisfaction of some desire. This is because a desire, i.e. lack, is the
  prior condition for every pleasure.

- Only lack, i.e. pain, is ever given to us directly. Our cognition of
  satisfaction and pleasure is only indirect, when we remember the sufferings
  and privations that preceded them and ceased when they appeared. That is why
  we do not really notice or value the possessions and the advantages we
  actually have, but think that they represent the necessary course of things:
  this is because they only make us happy negatively, by warding off suffering.
  We can only feel their value after we have lost them.

- all happiness is of a negative rather than positive nature, and for this
  reason cannot give lasting satisfaction and gratification, but rather only
  ever a release from a pain or lack, which must be followed either by a new
  pain or by languor, empty yearning and boredom.

- Viewed overall and in general manner, and extracting only the most
  significant features, the life of every individual is in fact always a
  tragedy; but worked through in detail, it has the character of a comedy.

(§59)
- the much-lamented brevity of life might be the best thing about it.

- if we were to call everyone's attention to the terrible pains and suffering
  their lives are constantly exposed to, they would be seized with horror: and
  if you led the most unrepentant optimist through the hospitals, military
  wards, and surgical theatres, through the prisons, torture chambers and
  slave stalls, through battlefields and places of judgement, and then open
  for him all the dark dwellings of misery that hide from cold curiosity,
  and finally let him peer into Ugolino's starvation chamber, then he
  too would surely come to see the nature of this best of all possible
  worlds

- as with all inferior goods, human life is covered with false glitter on the
  outside: what suffers always hides itself; and conversely, people like to
  show off whatever glamour and glitter they can afford, and the more that
  inner contentment eludes them, the more they want other people to think of
  them as happy: this is how far stupidity will go, and other people's opinion
  is a principal goal of everyone's efforts, although the total nothingness of
  this is already apparent from the fact that in almost all languages, vanity,
  vanitas, originally meant emptiness and nothingness.

- optimism, where it is not just the thoughtless talk of someone with only
  words in his flat head, strikes me as not only absurd, but even a truly
  wicked way of thinking, a bitter mockery of the unspeakable sufferings of
  humanity. 

- Do not think for a moment that Christian doctrine is favourable to optimism;
  on the contrary, in the Gospels, 'world' and 'evil' are used as almost
  synonymous expressions.

(§68)
- every fulfilled wish we wrest from the world is really like alms that keep
  the beggar alive today so that he can starve again tomorrow.

(§69)
Starts speaking about suicide.

(§71)
- for everyone who is still filled with the will, what remains after it is
  completely abolished is certainly nothing. But conversely, for those in whom
  the will has turned and negated itself, this world of ours which is so very
  real with all its suns and galaxies is — nothing.
