Libro II

Al despuntar la aurora, hazte estas consideraciones previas: me encontraré con
un indiscreto, un ingrato, un insolente, un mentiroso, un envidioso, un
insociable. Todo eso les acontece por ignorancia de los bienes y de los males.
Pero yo, que he observado que la naturaleza del bien es lo bello, y que la del
mal es lo vergonzoso, y que la naturaleza del pecador mismo es pariente de la
mía, porque participa, no de la misma sangre o de la misma semilla, sino de la
inteligencia y de una porción de la divinidad, no uedo recibir daño de ninguno
de ellos, pues ninguno me cubrirá de vergüenza; ni puedo enfadarme con mi
pariente ni odiarle. Pueso hemos nacido para colaborar [..] Obrar, pues, como
adversarios los unos de los otros es contrario a la naturaleza.


¿Qué es la muerte? Porque si se la mira a ella exclusivamente y se abstraen, por
división de su concepto, los fantasmas que la recubren, a no sugerirá otra cosa
sino que es obra de la naturaleza. Y si alguien teme la acción de la naturaleza,
es un chiquillo.
