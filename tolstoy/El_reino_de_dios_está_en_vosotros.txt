se ha llegado a una fe absolutamente incomprensible, llevada hasta tal absurdo
que exige creer a ciegas no en Dios, ni en Cristo, ni incluso en la doctrina,
sino en cierto individuo (como ocurre enel catolicismo), en varios individuos
(como ocurre en la ortodoxia), o en un libro (como ocurre en el protestantismo).
[..]
se debía creer que las personas que se hacían llamar "Iglesia" eran del todo
incuestionales, con lo que el sentido del cristianismo se redujo a que no
debíamos creer en Dios, ni en Cristo -tal y como nos fue anunciado-, sino
únicamente en la Iglesia y en lo que ésta nos ordenaba que creyéramos.
[..]
No encontramos referencia alguna en ningún lugar -aparte de las afirmaciones de
la propia Iglesia- que nos haga pensar que Dios o Jesucristo fundaran algo
parecido a lo que los eclesiásticos entienden por Iglesia.
[..]
En los Evangelios se utiliza el término "Iglesia" en dos ocasiones. La primera
tiene el significado de una reunión de personas que discuten un problema; la
segunda se utiliza en relación con las palabras -poco claras- sobre la piedra,
Pedro y las puertas del infierno. De estas dos menciones de la palabra
"Iglesia", que tienen un sentido estrictamente de reunión, se ha extraído el
sentido que hoy le otorgamos al término "Iglesia".

[..] etc.
