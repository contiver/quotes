good writers always take great pains to urge their readers to think exactly
what they themselves have thought; for whoever has something important to
communicate will be anxious that it not be lost. Therefore, good style rests
mainly on the fact that one really has something to say; it is just this small
detail that is lacking in most contemprary writers and thus is to blame for
their bad delivery.
    -- Arthur Schopenhauer. Parerga (On university philosophy)

noticing and rebuking our mistakes in others is a very useful means of becoming
aware of them in ourselves. We need a mirror in order to improve.  The same rule
also applies in regard to style and manner of writing: Whoever admires a new
folly in them instead of censuring it is going to imitate it.
    -- Arthur Schopenhauer. Parerga (Aphorisms on the wisdom of life)

Cuando se trata de una imagen, nada temerá más el escritor cuidadoso que el
dejar en la duda al lector o el inducirlo a extravío: pues lo que la imagen
debe hacer es aclarar las cosas; pero si la imagen misma está expresada de una
manera nada clara e induce a extravío, entonces lo que hace es oscurecer la
cosa, volverla más confusa de lo que era sin imagen.
    -- Friedrich Nietzsche. Consideraciones intempestivas 1 (Doce)

La receta, por ejemplo, para llegar a ser un buen novelista es fácil de dar,
pero la ejecución presuone cualidades que suelen asarse por alto cuando se
dice: «no tengo suficiente talento». Hágase más de cien bosquejos de novelas,
ninguno de más de dos áginas, pero de tal concisión que cada palabra sea
necesaria; anótese anécdotas diariamente hasta que se aprenda a encontrar su
forma más escueta, más eficaz, séase infatigable en la recopilación y
descripción de tipos y caracteres humanos, relátese ante todo con tanta
frecuencia como sea posible y óigase relatar con vista y oído aguzados para el
efecto sobre los demás circunstantes, viájese como paisajista o un figurinista,
extráigase de cada una de las ciencias todo lo que produce efectos artísticos
cuando se expone bien, medítese finalmente sobre los motivos de las acciones
humanas, no se desdeñe ninguna indicación instructiva a este respecto y séase
recolector de semejantes cosas día y noche. Pásese en este múltiple ejercicio
unos diez años; entonces lo que se crea en el taller puede salir también a la
luz pública.
    -- Friedrich Nietzsche. Humano, demasiado humano (163)

Un buen escritor tiene no sólo su propio espíritu, sino también el espíritu de
sus amigos.
    -- Friedrich Nietzsche. Humano, demasiado humano (180)

A un escritor debería considerársele como a un malhechor que sólo en los más
raros casos merece la absolución o el indulto: este sería un remedio contra la
proliferación de libros.
    -- Friedrich Nietzsche. Humano, demasiado humano (193)

The pleasure that a writer provides always requires a certain harmony between
his manner of thinking and the reader's, and will be all the greater the more
complete this harmony is; which is why a great intellect can only be enjoyed
completely and perfectly by another great intellect. Precisely upon this rest
the disgust and aversion that bad or mediocre writers bring forht in thinking
minds.
    -- Arthur Schopenhauer. Paralipomena §48

For artists, poets and writers generally other things also belong to the
subjective impurities of the intellect, namely what we like to call ideas of
the age or nowadays 'period consciousnes', in other words certain views and
concepts that are in vogue. The write tainted by their colour has allowed
himself to be impressed by them, instead of overlooking and rejecting them. Now
when those views have entirely disappeared and died out, after a shorter or
longer series of years, suddenly his workds originating from that period lack
the support they had in these views, and often they appear inconceivably
insipid, in any case like an old calendar. Only the entirely genuine poet or
thinker is above all such influences. Even Schiller looked into The Critique of
Practical Reason and it impressed him; buy Shakespeare simply looked into the
world. That is why in all his dramas but most distinctly in the English
historical ones we find the charaters generally set in motion by motives of
self-interest or malice, with few and not too glaring exceptions. He wanted to
portray the human being in the mirror of poetic art, not moral caricature,
hence everyone recognizes them in the mirror and his works live today and
evermore. The Schillerian characters in Don Carlos can be fairly sharply
divided into white and black, angels and devils. Already now they seem peculiar
- what will become of them after fifty years!
    -- Arthur Schopenhauer. Paralipomena §49

